# Semai



A native iOS App that helps in monitoring and analyzing hydroponics' growth.

This app is for beginner hydroponics gardeners, who use wicking system, to monitor & analyze their plant based on the combination of  photo, days after planting and nutrient solution. This app will also provide suggestions based on the results of the analysis. This is a solution to the challenge because it helps them to understand their plants growth and needs.

## Design

[Sources Sketch](https://sketch.com/s/f748bf73-047c-4d3b-b10c-fa22c46607f5)

## Sources Codes

[Gitlab](https://gitlab.com/nvnthermawan12/semai)


## Etc

[Hail hydro miro](https://miro.com/app/board/o9J_l9E3Emw=/)

