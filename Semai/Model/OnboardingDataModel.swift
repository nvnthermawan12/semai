//
//  OnboardingDataModel.swift
//  Semai
//
//  Created by Octavianus Bastian on 25/07/21.
//

import Foundation


struct OnboardingDataModel {
    var image: String
    var heading: String
    var text: String
}

struct OnBoardingManager {
    var onBoardings: [OnboardingDataModel] = [
        OnboardingDataModel(image: "onboarding-1", heading: "Plant Guidance", text: "Semai will guide you until you can harvest your own plants successfully. "),
        OnboardingDataModel(image: "onboarding-2", heading: "Plant Analysis", text: "Don’t be afraid if there’s something wrong on your plant. Take a photo of it and analyze with Semai!"),
        OnboardingDataModel(image: "onboarding-3", heading: "Plant History", text: "Semai will analyze your plant based on your plant photo and plant history. So, it will be more accurate."),
        OnboardingDataModel(image: "onboarding-1", heading: "Welcome to", text: ""),    ]
}
