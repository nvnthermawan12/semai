//
//  PlantDataModel.swift
//  Semai
//
//  Created by Octavianus Bastian on 29/07/21.
//

import Foundation

struct PlantDataModel {
    var image: String
    var plantName: String
}

struct PlantManager {
    var plants: [PlantDataModel] = [
        PlantDataModel(image: "test", plantName: "1st Mustard Green"),
        PlantDataModel(image: "test", plantName: "2nd Mustard Green"),
        PlantDataModel(image: "test", plantName: "3th Mustard Green"),
        PlantDataModel(image: "test", plantName: "4th Mustard Green"),    ]
}
