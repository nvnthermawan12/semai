//
//  PreparationTutorialModelData.swift
//  Semai
//
//  Created by Novianto Hermawan on 29/07/21.
//

import Foundation

struct PreparationTutorialModelData: Hashable {
    var image: String
    var textHeader: String
    var numberListTop: String
    var suggestionsWordTop: String
    var numberListBottom: String
    var suggestionsWordBottom: String
}

struct ModelDataManagerPreparationTutorial {
    var preparationTutorData: [PreparationTutorialModelData] = [
        PreparationTutorialModelData(image: "boxPrepare", textHeader: "Box Preparation", numberListTop: " 1 ", suggestionsWordTop: "Cut round holes in styrofoam box. Give space between holes about 10 - 15 cm.", numberListBottom: " 2 ", suggestionsWordBottom: "Cover the bottom side of styrofoam box with black plastic"),
        PreparationTutorialModelData(image: "rockwoolPrepare", textHeader: "Rockwool Preparation", numberListTop: " 3 ", suggestionsWordTop: "Cut rockwool into square about 2.5 x 2.5 cm with height about 5 cm.", numberListBottom: " 4 ", suggestionsWordBottom: "Soak all of the rockwool into water. Make sure that all parts get water."),
        PreparationTutorialModelData(image: "seedingPrepare", textHeader: "Seeding Preparation", numberListTop: " 5 ", suggestionsWordTop: "Put every seed near the each rockwool hole", numberListBottom: " 6 ", suggestionsWordBottom: "Keep it in black plastic and out it at dark room.")
    ]
}
