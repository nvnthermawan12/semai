//
//  PlantTypeModel.swift
//  Semai
//
//  Created by Felinda Gracia Lubis on 22/07/21.
//

import Foundation

struct PlantTypeModel: Hashable {
    var name: String
    var photo: String
    var harvestTime: Int
}

struct PlantTypeManager {
    var plants: [PlantTypeModel] = [
        PlantTypeModel(name: "Lettuce", photo: "lettuce-1", harvestTime: 40),
        PlantTypeModel(name: "Mustard Green", photo: "mustard green", harvestTime: 40),
        PlantTypeModel(name: "Bok Choy", photo: "bok choy", harvestTime: 40),
        PlantTypeModel(name: "Cucumber", photo: "cucumber", harvestTime: 40),
        PlantTypeModel(name: "Spinach", photo: "spinach", harvestTime: 40),
        PlantTypeModel(name: "Water Spinach", photo: "water spinach", harvestTime: 40)
    ]
}
