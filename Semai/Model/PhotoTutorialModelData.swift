//
//  PhotoTutorialModelData.swift
//  Semai
//
//  Created by Novianto Hermawan on 30/07/21.
//

import Foundation

struct PhotoTutorialModelData: Hashable {
    var image: String
    var suggestionsWord: String
}

struct ModelDataManagerPhotoTutorial {
    var photoTutorData: [PhotoTutorialModelData] = [
    PhotoTutorialModelData(image: "rightAnglePhoto", suggestionsWord: "Take every photo from top view and make sure that every part of the plat get captured."),
        PhotoTutorialModelData(image: "wrongAnglePhoto", suggestionsWord: "Don't take photo from the side of plant and avoid the plant photo from shadow, Semai won't be able to analyze all of the parts clarly.")
    ]
}
