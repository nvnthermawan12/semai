//
//  AddNutritionViewModel.swift
//  TextToSpeech
//
//  Created by Adhitya Laksamana Bayu Adrian on 28/07/21.
//

import Foundation
import CoreData

// MARK: - Gate for View Model to Manager
class AddNutritionViewModel: ObservableObject{
    var pPM: String = ""
    var imgbuff: String = ""
    var name: String = "Sawi"
    var nutritionPPM = ["0 PPM","300 PPM","800 PPM","1200 PPM"]
    @Published var nutritions: [NutritionViewModel] = []
    let container = PersistenceController.shared
        
//    func getAll(){
//        nutritions = AddNutritionManager.shared.getAllData().map(NutritionViewModel.init)
//    }
    
    func getAll() -> [PlantData] {
        let request: NSFetchRequest<PlantData> = PlantData.fetchRequest()
        do{
            return try container.container.viewContext.fetch(request)
        } catch {
            print(error)
            return[]
        }
    }
    
    func saveNutrition(){
//        let nutrition = Nutrition(context: AddNutritionManager.shared.container.viewContext)
        let nutrition = PlantData(context: container.container.viewContext)
        nutrition.ppm = pPM
        nutrition.name = name
        nutrition.imgBuffer = imgbuff
        print(nutrition)
//        AddNutritionManager.shared.save()
    }
}
// MARK: - Gate for View Model to View
struct NutritionViewModel{
    let nutrition: PlantData
    
    var pPM: String {
        return nutrition.ppm ?? "0 PPM"
    }
    var day: String{
        return nutrition.name ?? ""
    }
    var plant: String{
        return nutrition.imgBuffer ?? ""
    }

}
