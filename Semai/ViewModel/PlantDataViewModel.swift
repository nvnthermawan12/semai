//
//  PlantDataViewModel.swift
//  Semai
//
//  Created by Octavianus Bastian on 29/07/21.
//

import Foundation

class PlantDataViewModel {
    let plantManager = PlantManager()
    let plant: [PlantDataModel]
    
    init() {
        plant = plantManager.plants
    }
}
