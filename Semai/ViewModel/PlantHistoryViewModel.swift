//
//  PlantHistoryViewModel.swift
//  Semai
//
//  Created by Felinda Gracia Lubis on 28/07/21.
//

import Foundation
import CoreData

class PlantHistoryViewModel: ObservableObject {
    @Published var history: [PlantHistory] = []
    private let persistenceController = PersistenceController.shared
    
//    init() {
//        fetchData()
//    }
    
    func fetchData(_ myPlants: MyPlants) {
        let request = NSFetchRequest<PlantHistory>(entityName: "PlantHistory")
        let predicate = NSPredicate(format: "myPlants.name MATCHES %@", myPlants.name!)
        let sdSortDate = NSSortDescriptor(key: "journalDate", ascending: true)
        request.predicate = predicate
        request.sortDescriptors = [sdSortDate]
        do {
            history = try persistenceController.container.viewContext.fetch(request)
        } catch let error {
            print("Error fetching \(error)")
        }
    }
    
    func getDay(index: Int) -> Int? {
        let calendar = NSCalendar.current
        guard let dateAdded = history[index].myPlants?.dateAdded, let toDate = history[index].journalDate else {return 1}
        
        let day1 = calendar.startOfDay(for: dateAdded)
        let day2 = calendar.startOfDay(for: toDate)
        
        let day = calendar.dateComponents([.day], from: day1, to: day2).day
        return day
    }
    
}
