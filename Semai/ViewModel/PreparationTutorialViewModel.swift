//
//  PreparationViewModel.swift
//  OnBoarding
//
//  Created by Novianto Hermawan on 27/07/21.
//

import Foundation

class PreparationTutorialViewModel{
    let dataManager = ModelDataManagerPreparationTutorial()
    let preparationListData: [PreparationTutorialModelData]
    
    init() {
        preparationListData = dataManager.preparationTutorData
    }
}
