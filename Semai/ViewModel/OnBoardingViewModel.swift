//
//  OnBoardingViewModel.swift
//  Semai
//
//  Created by Octavianus Bastian on 28/07/21.
//

import Foundation

class OnBoardingViewModel {
    let onBoardingManager = OnBoardingManager()
    let onboarding: [OnboardingDataModel]
    
    init() {
        onboarding = onBoardingManager.onBoardings
    }
}
