//
//  PhotoTutorViewModel.swift
//  Semai
//
//  Created by Novianto Hermawan on 30/07/21.
//

import Foundation

class PhotoTutorViewModel{
    let dataManager = ModelDataManagerPhotoTutorial()
    let photoTutorListData: [PhotoTutorialModelData]
    
    init() {
        photoTutorListData = dataManager.photoTutorData
    }
}
