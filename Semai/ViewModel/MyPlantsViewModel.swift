//
//  MyPlantsViewModel.swift
//  Semai
//
//  Created by Felinda Gracia Lubis on 22/07/21.
//

import Foundation
import CoreData

class MyPlantsViewModel : ObservableObject {
    var plantTypeManager = PlantTypeManager()
    var plantType: [PlantTypeModel]
    @Published var filteredPlant: [MyPlants] = []
    @Published var myPlants: [MyPlants] = []
    let persistenceController = PersistenceController.shared
    
    init() {
        plantType = plantTypeManager.plants
        fetchData()
    }
    
    func filterMyPlant(index: Int) {
        filteredPlant.removeAll()
        for plant in myPlants {
            // ganti dengan data my plants dari database
            if plant.plantType == index {
                filteredPlant.append(plant)
            }
        }
    }
    
    func removeFilter() {
        filteredPlant.removeAll()
        filteredPlant = myPlants
    }
    
    func dayToHarvest(_ plant: MyPlants) -> Int {
        //        fetchData(plant)
        let calendar = NSCalendar.current
        guard let dateAdded = plant.dateAdded else {return 0}
        
        let last = calendar.startOfDay(for: Date())
        let harvestDay = calendar.date(byAdding: .day, value: 40, to: Calendar.current.date(bySettingHour: 12, minute: 0, second: 0, of: dateAdded)!)!
        
        let harvestLeft = calendar.dateComponents([.day], from: last, to: harvestDay).day!
        print(harvestLeft)
        return harvestLeft
    }
    
    func fetchData() {
        let request = NSFetchRequest<MyPlants>(entityName: "MyPlants")
        do {
            myPlants = try persistenceController.container.viewContext.fetch(request)
            filteredPlant = myPlants
        } catch {
            fatalError("Failed to fetch data: \(error)")
        }
    }
    
    func addMyPlants(plantType: Int, plantName: String) {
        let newItem = MyPlants(context: persistenceController.container.viewContext)
        newItem.id = UUID()
        newItem.name = plantName
        newItem.dateAdded = Date()
        newItem.plantType = Int16(plantType)
        save()
        addDummyHistory()
    }
    
    func addDummyHistory() {
        for i in 1...9 {
            let newDummy = PlantHistory(context: persistenceController.container.viewContext)
            newDummy.journalDate = Calendar.current.date(byAdding: .day, value: i, to: Calendar.current.date(bySettingHour: 12, minute: 0, second: 0, of: Date())!)
            newDummy.myPlants = myPlants[0]
            save()
            addDummyPlant(history: newDummy)
        }
    }
    
    func addDummyPlant(history: PlantHistory) {
        let newDummy = PlantData(context: persistenceController.container.viewContext)
        newDummy.dateHistory = history
        newDummy.name = "Cucumber 1"
        newDummy.imgBuffer = "cucumber"
        newDummy.ppm = "300"
        save()
    }
    
    func save() {
        do {
            try persistenceController.container.viewContext.save()
            fetchData()
        } catch {
            let nsError = error as NSError
            fatalError("Unresolved error \(nsError), \(nsError.userInfo)")
        }
    }
}
