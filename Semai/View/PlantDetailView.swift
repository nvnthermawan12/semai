//
//  PlantDetailView.swift
//  Semai
//
//  Created by Octavianus Bastian on 29/07/21.
//

import SwiftUI

struct PlantDetailView: View {
    var body: some View {
        NavigationView{
            VStack {
                Image("test")
                    .resizable().scaledToFill().frame(width: 375, height: 375).cornerRadius(20).padding(.top, 30)
                
                HStack{
                    Text("PLANT CARE")
                        .font(.title3)
                        .fontWeight(.semibold).foregroundColor(Color(#colorLiteral(red: 0.4705882353, green: 0.4666666667, blue: 0.4666666667, alpha: 1)))
                    
                }.frame(minWidth: /*@START_MENU_TOKEN@*/0/*@END_MENU_TOKEN@*/,  maxWidth: /*@START_MENU_TOKEN@*/.infinity/*@END_MENU_TOKEN@*/, alignment: .leading).padding(.horizontal, 30).padding(.vertical, 15)
                
                VStack {
                    HStack {
                        VStack {
                            Image(systemName: "drop.fill").foregroundColor(.white).font(.callout)}.frame(width: 33, height: 33, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/).background(Color("lightGreen")).cornerRadius(33 / 2)
                        
                        Text("Your current plant nutrition is 800 PPM, change your nutrition to 1200 PPM").font(.callout).fontWeight(.regular).foregroundColor(.black)
                    }.frame(minWidth: /*@START_MENU_TOKEN@*/0/*@END_MENU_TOKEN@*/,  maxWidth: /*@START_MENU_TOKEN@*/.infinity/*@END_MENU_TOKEN@*/, alignment: .leading).padding(.horizontal, 30)
                    
                    HStack {
                        VStack {
                            Image(systemName: "sun.max.fill").foregroundColor(.white).font(.callout)}.frame(width: 33, height: 33, alignment: .center).background(Color("lightGreen")).cornerRadius(33 / 2)
                        
                        Text("Plants need 6 hours or more sun light.").font(.callout).fontWeight(.regular).foregroundColor(.black)
                    }.padding(.vertical, 5).frame(minWidth: /*@START_MENU_TOKEN@*/0/*@END_MENU_TOKEN@*/,  maxWidth: /*@START_MENU_TOKEN@*/.infinity/*@END_MENU_TOKEN@*/, alignment: .leading).padding(.horizontal, 30)
                    
                    HStack {
                        VStack {
                            Image(systemName: "leaf.fill").foregroundColor(.white).font(.callout)}.frame(width: 33, height: 33, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/).background(Color("lightGreen")).cornerRadius(33 / 2)
                        
                        Text("Plants are healthy, there’s no insects.  ").font(.callout).fontWeight(.regular).foregroundColor(.black)
                    }.frame(minWidth: /*@START_MENU_TOKEN@*/0/*@END_MENU_TOKEN@*/,  maxWidth: /*@START_MENU_TOKEN@*/.infinity/*@END_MENU_TOKEN@*/, alignment: .leading).padding(.horizontal, 30)
                }
                
                Spacer()
            }.navigationTitle("1st Mustard Green")
            .navigationBarBackButtonHidden(false)
            .navigationBarTitleDisplayMode(.inline)
        }
        
    }
}

struct PlantDetailView_Previews: PreviewProvider {
    static var previews: some View {
        PlantDetailView()
    }
}
