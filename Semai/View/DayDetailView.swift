//
//  BatchDetailView.swift
//  Semai
//
//  Created by Octavianus Bastian on 28/07/21.
//

import SwiftUI

struct DayDetailView: View {
    var plantDataVM = PlantDataViewModel()
    var plantGrid: [GridItem] {
        [GridItem(.flexible()), GridItem(.flexible())]
    }
    @State var show = false
    @Binding var showChoosePlant: Bool
    @ObservedObject var myPlantsVM: MyPlantsViewModel
    
    
    @State var selectedId: Int?
    var body: some View {
        NavigationView {
            ZStack {
                Color(#colorLiteral(red: 0.9725490196, green: 0.9725490196, blue: 0.9725490196, alpha: 1)).ignoresSafeArea()
                VStack {
                    ScrollView(.vertical, showsIndicators: false, content: {
                        LazyVGrid(columns: plantGrid, content: {
                            Button(action: {
                                self.show.toggle()
                            }, label: {
                                AddNewCardView()
                            })
                            ForEach(plantDataVM.plant.indices, id: \.self){ i in
                                PlantCardView(data: self.plantDataVM.plantManager, index: i)
                            }
                        })
                    })
                }.padding()
                .navigationTitle("Day 10 Analysis")
                .navigationBarBackButtonHidden(false)
                .navigationBarTitleDisplayMode(.inline)
            }
        }
        .sheet(isPresented: self.$show){
            AddPlantNameView(showNameView: $showChoosePlant, selectedPlantId: selectedId ?? 0, myPlantsVM: myPlantsVM)
        }
        
    }
}

//struct DayDetailView_Previews: PreviewProvider {
//    static var previews: some View {
//        DayDetailView(showChoosePlant: false, myPlantsVM: MyPlantsViewModel)
//    }
//}
//
