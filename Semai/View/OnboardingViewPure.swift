//
//  OnboardingViewPure.swift
//  Semai
//
//  Created by Octavianus Bastian on 25/07/21.
//

import SwiftUI

struct OnboardingViewPure: View {
    var onBoardingVM = OnBoardingViewModel()
    //    var data: [OnboardingDataModel]
//    var doneFunction: () -> ()
    
    @State var slideGesture: CGSize = CGSize.zero
    @State var curSlideIndex = 0
    var distance: CGFloat = UIScreen.main.bounds.size.width
    
    @Environment(\.presentationMode) var presentationMode: Binding<PresentationMode>
    
    func nextButton() {
        if self.curSlideIndex == self.onBoardingVM.onboarding.count - 1 {
            self.presentationMode.wrappedValue.dismiss()
            return
        }
        
        if self.curSlideIndex < self.onBoardingVM.onboarding.count - 1 {
            withAnimation {
                self.curSlideIndex += 1
            }
        }
    }
    
    func skipButton() {
        self.curSlideIndex = 3
    }
    
    
    var body: some View {
        ZStack {
            Color(#colorLiteral(red: 0.9725490196, green: 0.9725490196, blue: 0.9725490196, alpha: 1)).edgesIgnoringSafeArea(.all)
            
            ZStack(alignment: .center) {
                ForEach(0..<onBoardingVM.onboarding.count) { i in
                    OnboardingStepView(data: self.onBoardingVM.onBoardingManager, index: i)
                        .offset(x: CGFloat(i) * self.distance)
                        .offset(x: self.slideGesture.width - CGFloat(self.curSlideIndex) * self.distance)
                        .animation(.spring())
                        .gesture(DragGesture().onChanged{ value in
                            self.slideGesture = value.translation
                        }
                        .onEnded{ value in
                            if self.slideGesture.width < -50 {
                                if self.curSlideIndex < self.onBoardingVM.onboarding.count - 1 {
                                    withAnimation {
                                        self.curSlideIndex += 1
                                    }
                                }
                            }
                            if self.slideGesture.width > 50 {
                                if self.curSlideIndex > 0 {
                                    withAnimation {
                                        self.curSlideIndex -= 1
                                    }
                                }
                            }
                            self.slideGesture = .zero
                        })
                }
            }
            
            
            VStack {
                Spacer()
                Button(action: nextButton) {
                    self.continueButton()
                }
                
                Button(action: skipButton, label: {
                    Text("Skip")
                        .font(.body)
                        .fontWeight(.heavy).foregroundColor(Color("primaryGreen"))
                }).padding(.bottom, 20)

                
                self.progressView()
            }
            .padding(.bottom, 20)
        }
    }
    
    func continueButton() -> some View {
        Group {
            if self.curSlideIndex == self.onBoardingVM.onboarding.count - 1 {
                HStack {
                    Text("Get Started")
                        .bold()
                        .padding(14)
                        .padding(.horizontal, 45)
                        .foregroundColor(.white)
                        .background(Color("primaryGreen"))
                        .cornerRadius(25)
                        .padding(16)
                }
            } else {
                Text("Continue")
                    .bold()
                    .padding(14)
                    .padding(.horizontal, 45)
                    .foregroundColor(.white)
                    .background(Color("primaryGreen"))
                    .cornerRadius(25)
                    .padding(16)
            }
        }
    }
    
    func progressView() -> some View {
        HStack {
            ForEach(0..<onBoardingVM.onboarding.count) { i in
                Circle()
                    .scaledToFit()
                    .frame(width: 10)
                    .foregroundColor(self.curSlideIndex >= i ? Color("primaryGreen") : Color(.systemGray))
            }
        }
    }
    
}

struct OnboardingViewPure_Previews: PreviewProvider {
    static var previews: some View {
        OnboardingViewPure()
    }
}


