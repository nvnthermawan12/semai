//
//  EquipmentView.swift
//  Semai
//
//  Created by Octavianus Bastian on 23/07/21.
//

import SwiftUI

struct ChecklistItem: Identifiable {
    let id = UUID()
    var name: String
    var isChecked: Bool = false
}

struct EquipmentView: View {
    @Binding var showEquipmentView: Bool
    @State var checklistItems = [
        ChecklistItem(name: "Rockwool"),
        ChecklistItem(name: "Fruit styrofoam box"),
        ChecklistItem(name: "A & B plant nutrition"),
        ChecklistItem(name: "Mustard green seeds"),
        ChecklistItem(name: "Plant netpot"),
        ChecklistItem(name: "Black plastic"),
        ChecklistItem(name: "Flannel"),
        ChecklistItem(name: "Black duct tape"),
    ]
    
    @State var allChecked: Bool = false
    @Environment(\.presentationMode) var presentationMode
    
    var body: some View {
        
        ZStack {
            Color(#colorLiteral(red: 0.9725490196, green: 0.9725490196, blue: 0.9725490196, alpha: 1)).ignoresSafeArea()
            VStack {
                HStack {
                    Text("3 / 4")
                        .font(.footnote)
                        .fontWeight(.semibold)
                        .foregroundColor(Color("primaryGreen"))
                }.padding(.top, 20).padding(.bottom, 6)
                
                HStack {
                    Text("Prepare the equipments.")
                        .font(.title3)
                        .fontWeight(.semibold)
                        .foregroundColor(Color.black)
                }.padding(.bottom, 20)
                
                List {
                    ForEach(checklistItems) { checklistItem in
                        HStack {
                            Image(systemName: checklistItem.isChecked ? "largecircle.fill.circle" : "circle")
                                .foregroundColor(checklistItem.isChecked ? Color("primaryGreen") : Color.secondary)
                            Text(checklistItem.name)
                                .font(.callout)
                        }.onTapGesture {
                            if let matchingIndex =
                                self.checklistItems.firstIndex(where: { $0.id == checklistItem.id }) {
                                self.checklistItems[matchingIndex].isChecked.toggle()
                                checklist()
                            }
                        }
                    }
                }.cornerRadius(15).padding(.horizontal, 15).shadow(color: Color(UIColor.systemGray2), radius: 4, x: 0, y: 2).frame(height: 351)
                
                
                
                VStack(alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/, content: {
                    Text("If your equipments aren’t complete,")
                        .font(.footnote)
                        .foregroundColor(Color.black)
                    HStack {
                        Button(action: /*@START_MENU_TOKEN@*/{}/*@END_MENU_TOKEN@*/, label: {
                            Text("click here").font(.footnote)
                                .fontWeight(.semibold)
                                .foregroundColor(Color("primaryGreen"))
                        })
                        
                        Text("to contact Graceful Hydroponics.")
                            .font(.footnote)
                            .foregroundColor(Color.black).padding(.leading, -5) }.padding(.top, -5)
                    
                }).padding(.top, 25).padding(.bottom, 10)
                
                HStack {Spacer()
                    Button(action: {self.presentationMode.wrappedValue.dismiss()}, label: {
                        Text("Back")
                            .bold()
                            .padding(14)
                            .padding(.horizontal, 45)
                            .foregroundColor(Color("primaryGreen"))
                            .background(
                                Color.white
                            )
                            .cornerRadius(25)
                    }).shadow(color: Color(UIColor.systemGray2), radius: 4, x: 0, y: 1.5)
                    Spacer()
                    Button(action: {showEquipmentView = false}, label: {
                        Text("Next")
                            .bold()
                            .padding(14)
                            .padding(.horizontal, 45)
                            .foregroundColor(.white)
                            .background( allChecked ? Color( "primaryGreen" ) : Color.gray)
                            .cornerRadius(25)
                    })
                    Spacer()
                }.padding(16)
                Spacer()
            }
        }
        .navigationBarBackButtonHidden(true)
        .toolbar(content: {
            ToolbarItem(placement: .navigationBarLeading) {
                Button(action: {
                    showEquipmentView = false
                }, label: {
                    Text("Cancel")
                })
            }
        })
    }
    
    func checklist() {
        for item in checklistItems {
            if item.isChecked{
                self.allChecked = true
            }
            else {self.allChecked = false}
            
        }
    }
}
//struct EquipmentView_Previews: PreviewProvider {
//    static var previews: some View {
//        EquipmentView(showEquipmentView: .constant(true))
//    }
//}
