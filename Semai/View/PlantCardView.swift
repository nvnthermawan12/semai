//
//  PlantCardView.swift
//  Semai
//
//  Created by Octavianus Bastian on 29/07/21.
//

import SwiftUI

struct PlantCardView: View {
    var data: PlantManager
    var index: Int
    var body: some View {
        VStack{
            Image(data.plants[index].image)
                .resizable().scaledToFill().frame(width: 155, height: 155).cornerRadius(15)
            
            Text(data.plants[index].plantName)
                .font(.callout)
                .fontWeight(.regular)
                .foregroundColor(.black).padding(.bottom, 5)
        }
        .frame(width: 155, height: 190, alignment: .center).background(Color(#colorLiteral(red: 1, green: 1, blue: 1, alpha: 1))).cornerRadius(15).shadow(color: Color(UIColor.systemGray2), radius: 4, x: 0, y: 1.5).padding()
    }
}

struct PlantCardView_Previews: PreviewProvider {
    static var data = PlantManager()
    static var previews: some View {
        PlantCardView(data: data, index: 0).previewLayout(.fixed(width: 180, height: 250))
            .padding()
    }
}
