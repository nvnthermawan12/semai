//
//  PlantHistoryView.swift
//  Semai
//
//  Created by Felinda Gracia Lubis on 28/07/21.
//

import SwiftUI

struct PlantHistoryView: View {
    @State var selectedPlant: MyPlants?
    //    @State var descending: Bool
    @ObservedObject var vm: PlantHistoryViewModel
    
    var body: some View {
        ZStack {
            Color(#colorLiteral(red: 0.9725490196, green: 0.9725490196, blue: 0.9725490196, alpha: 1)).ignoresSafeArea()
            VStack (alignment: .leading) {
                if !vm.history.isEmpty {
                    Section(header: Text("SEEDING").bold().foregroundColor(.gray).padding(.top, 14)) {
                        Text("Day - \(vm.getDay(index: 0) ?? 1)")
                            .padding(14)
                            .frame(minWidth: 0, maxWidth: .infinity, alignment: .leading)
                            .background(Color.white)
                            .cornerRadius(16)
                            .shadow(color: Color(UIColor.systemGray2), radius: 4, x: 0, y: 1.5)
                    }
                    .padding(.horizontal, 20)
                    .padding(.vertical, 4)
                    Section(header: Text("GROWING").bold().foregroundColor(.gray)) {
                        ForEach(vm.history.indices) { i in
                            Text("Day - \(vm.getDay(index: i) ?? 1)")
                                .padding(14)
                                .frame(minWidth: 0, maxWidth: .infinity, alignment: .leading)
                                .background(Color.white)
                                .cornerRadius(16)
                                .shadow(color: Color(UIColor.systemGray2), radius: 4, x: 0, y: 1.5)
                        }
                    }
                    .padding(.vertical, 4)
                    .padding(.horizontal, 20)
                    Spacer()
                }
            }
        }
        .navigationTitle(selectedPlant?.name ?? "")
        .navigationBarTitleDisplayMode(.inline)
        .toolbar(content: {
            ToolbarItem(placement: .navigationBarTrailing) {
                HStack {
                    Button(action: {
                    }, label: {
                        Image(systemName: "arrow.up.arrow.down.circle.fill")
                            .foregroundColor(.white)
                            .font(.system(size: 24))
                    })
                    Button(action: {
                    }, label: {
                        Image(systemName: "plus.circle.fill")
                            .foregroundColor(.white)
                            .font(.system(size: 24))
                    })
                }
            }
        })
    }
}

//struct PlantHistoryView_Previews: PreviewProvider {
//    static var previews: some View {
//        PlantHistoryView()
//    }
//}
