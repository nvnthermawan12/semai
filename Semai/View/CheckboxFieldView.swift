//
//  CheckboxFieldView.swift
//  Semai
//
//  Created by Octavianus Bastian on 23/07/21.
//

import SwiftUI



struct CheckboxFieldView: View {
    //    @State var checkState:Bool = false ;
    
    @Binding var checked: Bool
    
    var body: some View {
        

        Image(systemName: checked ? "largecircle.fill.circle" : "circle")
                    .foregroundColor(checked ? Color("primaryGreen") : Color.secondary)
                    .onTapGesture {
                        self.checked.toggle()
                    }
    }
    
}



struct CheckboxFieldView_Previews: PreviewProvider {
    struct CheckBoxViewHolder: View {
        @State var checked = false
        
        var body: some View {
            CheckboxFieldView(checked: $checked)
        }
    }
    
    static var previews: some View {
        CheckBoxViewHolder()
    }
}
