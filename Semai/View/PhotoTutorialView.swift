//
//  PhotoTutorialView.swift
//  Semai
//
//  Created by Novianto Hermawan on 30/07/21.
//

import SwiftUI

struct PhotoTutorialView: View {
    var photoVM = PhotoTutorViewModel()
    
    init(){
        UIPageControl.appearance().currentPageIndicatorTintColor = .black
        UIPageControl.appearance().pageIndicatorTintColor =
            UIColor.black.withAlphaComponent(0.2)
    }
    
    var body: some View {
        ZStack{
            Color(#colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)).ignoresSafeArea()
            VStack {
                HStack {
                    Text("How to take a plant photo")
                        .font(.system(size: 20))
                        .fontWeight(.semibold)
                }.padding(.top, 20)
                TabView{
                    ZStack{
                        RoundedRectangle(cornerRadius: 15, style: .continuous).fill(Color.white).padding(25).shadow(color:Color(UIColor.systemGray2) ,radius: 4)
                        VStack {
                            Image(photoVM.photoTutorListData[0].image).resizable()
                                .scaledToFit()
                                .padding(.horizontal, 25).frame(height:300)
                            Text(photoVM.photoTutorListData[0].suggestionsWord).frame(width:310).padding(.top, 20).font(.system(size: 16))
                            
                        }
                        
                    }.frame(height:516)
                    ZStack{
                        RoundedRectangle(cornerRadius: 15, style: .continuous).fill(Color.white).padding(25).shadow(color:Color(UIColor.systemGray2) ,radius: 4)
                        VStack {
                            Image(photoVM.photoTutorListData[1].image).resizable()
                                .scaledToFit()
                                .padding(.horizontal, 25).frame(height:300)
                            Text(photoVM.photoTutorListData[1].suggestionsWord).frame(width:310).padding(.top, 20).font(.system(size: 16))
                            
                        }
                        
                        
                        
                    }.frame(height:516)
                }.tabViewStyle(PageTabViewStyle(indexDisplayMode: .always))
                HStack {Spacer()
                    Button(action: /*@START_MENU_TOKEN@*/{}/*@END_MENU_TOKEN@*/, label: {
                        Text("Back")
                            .bold()
                            .padding(14)
                            .padding(.horizontal, 45)
                            .foregroundColor(Color("primaryGreen"))
                            .background(
                                Color.white
                            )
                            .cornerRadius(25)
                    }).shadow(color: Color(UIColor.systemGray2), radius: 4, x: 0, y: 1.5)
                    Spacer()
                    Button(action: /*@START_MENU_TOKEN@*/{}/*@END_MENU_TOKEN@*/, label: {
                        Text("Take a Photo")
                            .bold()
                            .padding(14)
                            .padding(.horizontal)
                            .foregroundColor(.white)
                            .background(Color("primaryGreen"))
                            .cornerRadius(25)
                        
                    })
                    Spacer()
                }.padding(16)
                
            }
            
            
        }
    }
}

struct PhotoTutorialView_Previews: PreviewProvider {
    static var previews: some View {
        PhotoTutorialView()
    }
}
