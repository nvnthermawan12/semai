//
//  AddNewCardView.swift
//  Semai
//
//  Created by Octavianus Bastian on 29/07/21.
//

import SwiftUI

struct AddNewCardView: View {
    var body: some View {
        
        VStack{
            Image(systemName: "plus.circle.fill").foregroundColor(Color("primaryGreen"))
            
            Text("Add New")
                .font(.body)
                .fontWeight(.semibold)
                .foregroundColor(Color("primaryGreen")).padding(.top, 1)
        }
        .frame(width: 155, height: 190, alignment: .center).background(Color(#colorLiteral(red: 0.9176470588, green: 0.9176470588, blue: 0.9176470588, alpha: 1))).cornerRadius(15).shadow(color: Color(UIColor.systemGray2), radius: 4, x: 0, y: 1.5)
        
    }
}

struct AddNewCardView_Previews: PreviewProvider {
    static var previews: some View {
        AddNewCardView().previewLayout(.fixed(width: 180, height: 250))
            .padding()
    }
}
