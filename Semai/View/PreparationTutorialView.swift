//
//  PreparationTutorialView.swift
//  OnBoarding
//
//  Created by Novianto Hermawan on 26/07/21.
//

import SwiftUI
import UIKit

struct PreparationTutorialView: View {
    init() {
        UIPageControl.appearance().currentPageIndicatorTintColor = .black
                  UIPageControl.appearance().pageIndicatorTintColor = UIColor.black.withAlphaComponent(0.2)
    }
    var testViewModel = PreparationTutorialViewModel()
    
    
    var body: some View {
        ZStack {
            Color(#colorLiteral(red: 0.9725490196, green: 0.9725490196, blue: 0.9725490196, alpha: 1)).ignoresSafeArea()
            VStack {
                HStack {
                    Text(" 4 / 4")
                        .font(.footnote)
                        .fontWeight(.semibold)
                        .foregroundColor(Color("primaryGreen"))
                        
                }.padding(.top, 20)
                TabView{
                    VStack{
                        HStack {
                            Text(testViewModel.preparationListData[0].textHeader)
                                .font(.title3)
                                .foregroundColor(.black)
                                .fontWeight(.semibold)
                        }
                        
                        ZStack {
                                RoundedRectangle(cornerRadius: 15, style: .continuous).fill(Color.white).padding(25).shadow(color: Color(UIColor.systemGray),radius: 4)
                                VStack {
                                    Image(testViewModel.preparationListData[0].image)
                                        .resizable()
                                        .cornerRadius(15, corners: [.topLeft, .topRight])
                                        .scaledToFit()
                                        .padding(.horizontal, 25)
                                    HStack {
                                        ZStack {
                                            Image(systemName: "circle.fill").resizable().frame(width:22, height: 22)
                                                .foregroundColor(Color("lightGreen"))
                                            
                                            Text(testViewModel.preparationListData[0].numberListTop)
                                                .font(.system(size: 12))
                                        }
                                        Text(testViewModel.preparationListData[0].suggestionsWordTop).frame(width: 292).font(.system(size: 16))
                                    }.padding(.top)
                                    
                                    Divider().frame(width: 292.0, height:5).padding(.horizontal, 5)
                                    
                                    HStack {
                                        ZStack {
                                            Image(systemName: "circle.fill").resizable().frame(width:22, height: 22)
                                                .foregroundColor(Color("lightGreen"))
                                                
                                            Text(testViewModel.preparationListData[0].numberListBottom)
                                                .font(.system(size: 12))
                                        }
                                        Text(testViewModel.preparationListData[0].suggestionsWordBottom).frame(width: 292).font(.system(size: 16))
                                    }
                                   
                                    Spacer()
                                }
                            
                            
                        }.frame(height: 450)
                    }
                    VStack{
                        HStack {
                            Text(testViewModel.preparationListData[1].textHeader)
                                .font(.title3)
                                .foregroundColor(.black)
                                .fontWeight(.semibold)
                        }
                        
                        ZStack {
                                RoundedRectangle(cornerRadius: 15, style: .continuous).fill(Color.white).padding(25).shadow(color: Color(UIColor.systemGray),radius: 4)
                                VStack {
                                    Image(testViewModel.preparationListData[1].image)
                                        .resizable()
                                        .cornerRadius(15, corners: [.topLeft, .topRight])
                                        .scaledToFit()
                                        .padding(.horizontal, 25)
                                    HStack {
                                        ZStack {
                                            Image(systemName: "circle.fill").resizable().frame(width:22, height: 22)
                                                .foregroundColor(Color("lightGreen"))
                                            
                                            Text(testViewModel.preparationListData[1].numberListTop)
                                                .font(.system(size: 12))
                                        }
                                        Text(testViewModel.preparationListData[1].suggestionsWordTop).frame(width: 292).font(.system(size: 16))
                                    }.padding(.top)
                                    
                                    Divider().frame(width: 292.0, height:5).padding(.horizontal, 5)
                                    
                                    HStack {
                                        ZStack {
                                            Image(systemName: "circle.fill").resizable().frame(width:22, height: 22)
                                                .foregroundColor(Color("lightGreen"))
                                                
                                            Text(testViewModel.preparationListData[1].numberListBottom)
                                                .font(.system(size: 12))
                                        }
                                        Text(testViewModel.preparationListData[1].suggestionsWordBottom).frame(width: 292).font(.system(size: 16))
                                    }
                                   
                                    Spacer()
                                }
                            
                            
                        }.frame(height: 450)
                    }
                    VStack{
                        HStack {
                            Text(testViewModel.preparationListData[2].textHeader)
                                .font(.title3)
                                .foregroundColor(.black)
                                .fontWeight(.semibold)
                        }
                        
                        ZStack {
                                RoundedRectangle(cornerRadius: 15, style: .continuous).fill(Color.white).padding(25).shadow(color: Color(UIColor.systemGray),radius: 4)
                                VStack {
                                    Image(testViewModel.preparationListData[2].image)
                                        .resizable()
                                        .cornerRadius(15, corners: [.topLeft, .topRight])
                                        .scaledToFit()
                                        .padding(.horizontal, 25)
                                    HStack {
                                        ZStack {
                                            Image(systemName: "circle.fill").resizable().frame(width:22, height: 22)
                                                .foregroundColor(Color("lightGreen"))
                                            
                                            Text(testViewModel.preparationListData[2].numberListTop)
                                                .font(.system(size: 12))
                                        }
                                        Text(testViewModel.preparationListData[2].suggestionsWordTop).frame(width: 292).font(.system(size: 16))
                                    }.padding(.top)
                                    
                                    Divider().frame(width: 292.0, height:5).padding(.horizontal, 5)
                                    
                                    HStack {
                                        ZStack {
                                            Image(systemName: "circle.fill").resizable().frame(width:22, height: 22)
                                                .foregroundColor(Color("lightGreen"))
                                                
                                            Text(testViewModel.preparationListData[2].numberListBottom)
                                                .font(.system(size: 12))
                                        }
                                        Text(testViewModel.preparationListData[2].suggestionsWordBottom).frame(width: 292).font(.system(size: 16))
                                    }
                                   
                                    Spacer()
                                }
                            
                            
                        }.frame(height: 450)
                    }
                   
                }
                .tabViewStyle(PageTabViewStyle(indexDisplayMode: .always))
                
                HStack {Spacer()
                    Button(action: /*@START_MENU_TOKEN@*/{}/*@END_MENU_TOKEN@*/, label: {
                        Text("Back")
                            .bold()
                            .padding(14)
                            .padding(.horizontal, 45)
                            .foregroundColor(Color("primaryGreen"))
                            .background(
                                Color.white
                            )
                            .cornerRadius(25)
                    }).shadow(color: Color(UIColor.systemGray2), radius: 4, x: 0, y: 1.5)
                    Spacer()
                    NavigationLink(
                        destination: AddNutritionView(),
                        label: {
                            Text("Start")
                                .bold()
                                .padding(14)
                                .padding(.horizontal, 45)
                                .foregroundColor(.white)
                                .background(Color("primaryGreen"))
                                .cornerRadius(25)
                        })
                    Spacer()
                }.padding(16)
                
                Spacer()
            }
          
        }
        
    }
}

extension View{
    func cornerRadius(_ radius: CGFloat, corners: UIRectCorner) -> some View {
            clipShape( RoundedCorner(radius: radius, corners: corners) )
        }
}

struct RoundedCorner: Shape {
    
    var radius: CGFloat = .infinity
    var corners: UIRectCorner = .allCorners
    
    func path(in rect: CGRect) -> Path {
        let path = UIBezierPath(roundedRect: rect, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        return Path(path.cgPath)
    }
}


struct PreparationTutorialView_Previews: PreviewProvider {
        static var previews: some View {
        PreparationTutorialView()
    }
}
