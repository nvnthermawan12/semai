//
//  ContentView.swift
//  Semai
//
//  Created by Novianto Hermawan on 16/07/21.
//

import SwiftUI
import CoreData

struct MyPlantsView: View {
    @StateObject var myPlantsVM = MyPlantsViewModel()
    @StateObject var historyVM = PlantHistoryViewModel()
    @State var searchText: String = ""
    @State private var isEditing = false
    @State var showChoosePlant = false
    @State var isFiltered = false
    @State var selectedId: Int?
    @State var showHistory = false
    @State var selectedPlant: MyPlants? = nil

    @State private var showOnBoarding = false
    
    func checkFirstLaunch() {
        
        let firstLaunch = UserDefaults.standard.string(forKey: "firstlaunch")
        
        if (firstLaunch == nil) {
            print("firstlaunch")
            self.showOnBoarding.toggle()
            UserDefaults.standard.set(showOnBoarding, forKey: "firstlaunch")
        } else {
            print("udahfirstlaunch")
        }
    }
    
    //MARK: Content View : body
    var body: some View {
        NavigationView {
            ZStack {
                Color(#colorLiteral(red: 0.9725490196, green: 0.9725490196, blue: 0.9725490196, alpha: 1)).ignoresSafeArea()
                if let safeSelected = selectedPlant {
                    NavigationLink(
                        destination: PlantHistoryView(selectedPlant: safeSelected, vm: historyVM), isActive: $showHistory) {
                        EmptyView()
                    }
                }
                VStack {
                    ScrollView(.vertical, showsIndicators: false) {
                        // Search bar & Filtering
                        VStack {
                            // Search bar
                            HStack {
                                TextField("Search...", text: $searchText)
                                    .padding(7)
                                    .padding(.horizontal, 30)
                                    .background(Color(.systemGray6))
                                    .cornerRadius(8)
                                    .overlay(
                                        HStack {
                                            Image(systemName: "magnifyingglass")
                                                .foregroundColor(Color(.systemGray2))
                                                .frame(minWidth: .none, maxWidth: .infinity,   alignment: .leading)
                                                .padding(.leading, 8)
                                            if isEditing {
                                                // button to reset textfield
                                                Button(action: {
                                                    self.searchText = ""
                                                }) {
                                                    Image(systemName: "multiply.circle.fill")
                                                        .foregroundColor(Color("primaryGreen"))
                                                        .padding(.trailing, 8)
                                                }
                                            }
                                        }
                                    )
                                    .padding(.horizontal, 20)
                                    .onTapGesture {
                                        self.isEditing = true
                                    }
                                    .transition(.move(edge: .trailing))
                                    .animation(.spring())
                                if isEditing {
                                    Button(action: {
                                        UIApplication.shared.sendAction(#selector(UIResponder.resignFirstResponder), to: nil, from: nil, for: nil)
                                        isEditing = false
                                    }) {
                                        Text("Cancel")
                                            .padding(.trailing, 15)
                                            .foregroundColor(.white)
                                    }
                                    .transition(.move(edge: .trailing))
                                    .animation(.spring())
                                }
                            }
                            .padding(.top, 15)
                            // Filtering collection view
                            ScrollView (.horizontal, showsIndicators: false, content: {
                                LazyHStack {
                                    Text("All")
                                        .font(.body)
                                        .padding(8)
                                        .padding(.horizontal, 16)
                                        .background(isFiltered ? Color.white : Color("lightGreen"))
                                        .cornerRadius(16)
                                        .onTapGesture {
                                            isFiltered = false
                                            myPlantsVM.removeFilter()
                                        }
                                    ForEach(myPlantsVM.plantType.indices) { index in
                                        Text(myPlantsVM.plantType[index].name)
                                            .font(.body)
                                            .padding(8)
                                            .padding(.horizontal, 8)
                                            .background(index == selectedId && isFiltered ? Color("lightGreen") : Color(.white))
                                            .cornerRadius(16)
                                            .onTapGesture {
                                                isFiltered = true
                                                selectedId = index
                                                myPlantsVM.filterMyPlant(index: index)
                                            }
                                    }
                                }
                                .frame(height: 50)
                                .padding(.horizontal, 20)
                                .padding(.vertical, 10)
                            })
                            
                        }
                        .background(Color("primaryGreen"))
                        // My plants list
                        if !myPlantsVM.filteredPlant.isEmpty {
                            // My plants cell
                            ForEach(
                                myPlantsVM.filteredPlant.filter({ searchText.isEmpty ? true : $0.name!.contains(searchText)})) { plant in
                                ZStack {
                                    HStack {
                                        VStack(alignment: .leading, spacing: 4) {
                                            Text(plant.name ?? "")
                                                .font(.system(size: 20))
                                                .fontWeight(.semibold)
                                            HStack {
                                                Image(systemName: "clock.arrow.2.circlepath")
                                                Text("\(myPlantsVM.dayToHarvest(plant)) days to harvest")
                                                    .font(.system(size: 12))
                                                    .offset(x: -2)
                                            }
                                        }
                                        .padding(.horizontal, 20)
                                        Spacer()
                                    }
                                    Image(myPlantsVM.plantType[Int(plant.plantType)].photo)
                                        .resizable()
                                        .zIndex(-10)
                                        .frame(width: 130, height: 120)
                                        .scaledToFit()
                                        .rotation3DEffect(
                                            .degrees(180), axis: (x: 0.0, y: 1.0, z: 0.0)
                                        )
                                        .offset(x: 125)
                                }
                                .background(Color.white)
                                .cornerRadius(16)
                                .shadow(color: Color(UIColor.systemGray2), radius: 4, x: 0, y: 1.5)
                                .padding(.horizontal, 20)
                                .onTapGesture {
                                    selectedPlant = plant
                                    historyVM.fetchData(plant)
                                    showHistory = true
                                    print(showChoosePlant)
                                }
                            }
                            .padding(.vertical, 6)
                        } else {
                            // illustration when My Plants data is not available
                            Image("No plants")
                                .resizable()
                                .scaledToFit()
                                .padding(.horizontal, 50)
                                .padding(.vertical, 30)
                            VStack (alignment: .center, spacing: 14) {
                                Text("You still don’t have any plants.")
                                    .bold()
                                    .font(.title2)
                                Text("Tap add button \(Image(systemName: "plus.circle.fill")) on the top right corner of this page to start a new plant. ")
                                    .font(.system(size: 14))
                                    .padding(.horizontal, 30)
                                    .multilineTextAlignment(.center)
                            }
                            .padding(.horizontal, 20)
                        }
                    }
                }
                .onTapGesture {
                    UIApplication.shared.sendAction(#selector(UIResponder.resignFirstResponder), to: nil, from: nil, for: nil)
                    isEditing = false
                }
            }
            .navigationBarTitle("My Plants")
            .toolbar(content: {
                ToolbarItem(placement: .navigationBarTrailing) {
                    Button(action: {
                        showChoosePlant = true
                    }, label: {
                        Image(systemName: "plus.circle.fill")
                            .foregroundColor(.white)
                            .font(.system(size: 25))
                    })
                    .sheet(isPresented: $showChoosePlant) {
                        ChoosePlantView(showChoosePlant: $showChoosePlant, myPlantsVM: myPlantsVM)
                    }
                }
            })
        }.sheet(isPresented: $showOnBoarding, content: { OnBoarding()
            .frame(maxWidth: .infinity, maxHeight: .infinity)
        })
        .onAppear(perform: {
            checkFirstLaunch()
            print(FileManager.default.urls(for: .documentDirectory, in: .userDomainMask))
        })
    }
}

//MARK: Content View Preview
//struct ContentView_Previews: PreviewProvider {
//    static var previews: some View {
//        MyPlantsView().environment(\.managedObjectContext, PersistenceController.preview.container.viewContext)
//    }
//}

struct OnBoarding: View {
    
    // Variable to Dismiss Modal
    
    
    var body: some View {
        
        OnboardingViewPure()
        
        
//        Text("Whats new in this version?")
//
//        // Dismiss Button
//        Button(action: { self.presentationMode.wrappedValue.dismiss() }, label: {
//            Text("Dismiss")
//        })
        
        
    }
}
