//
//  ChoosePlantView.swift
//  Semai
//
//  Created by Felinda Gracia Lubis on 20/07/21.
//

import SwiftUI

struct ChoosePlantView: View {
    
    @Binding var showChoosePlant: Bool
    @ObservedObject var myPlantsVM: MyPlantsViewModel
    
    var plantGrid: [GridItem] {
        [GridItem(.flexible()), GridItem(.flexible())]
    }
    @State var selectedId: Int?
    
    var body: some View {
        NavigationView {
            ZStack {
                Color(#colorLiteral(red: 0.9725490196, green: 0.9725490196, blue: 0.9725490196, alpha: 1)).ignoresSafeArea()
                VStack {
                    Text("1 / 4")
                        .foregroundColor(Color("primaryGreen"))
                        .font(.system(size: 12))
                        .padding(.top, 25)
                        .padding(.bottom, 8)
                    Text("What do you want to plant?")
                        .bold()
                        .padding(.bottom, 25)
                    // Plant type picker
                    ScrollView(.vertical, showsIndicators: false, content: {
                        LazyVGrid(columns: plantGrid, content: {
                            ForEach(myPlantsVM.plantType.indices, id: \.self) { id in
                                VStack {
                                    Image(myPlantsVM.plantType[id].photo)
                                        .resizable()
                                        .frame(width: UIScreen.screenWidth/2 - 50, height: UIScreen.screenHeight/3 - 150)
                                        .scaledToFit()
                                    //                                    .cornerRadius(16)
                                    Text(myPlantsVM.plantType[id].name)
                                        .padding(.vertical, 10)
                                }
                                .overlay(
                                    RoundedRectangle(cornerRadius: 16)
                                        .stroke(Color("lightGreen"), lineWidth: 6)
                                        .opacity(selectedId == id ? 1 : 0)
                                )
                                .background(Color.white)
                                .cornerRadius(16)
                                .shadow(color: Color(UIColor.systemGray2), radius: 4, x: 0, y: 1.5)
                                .padding(.vertical, 8)
                                .padding(.horizontal, 8)
                                .onTapGesture {
                                    selectedId = id
                                }
                            }
                        })
                        .padding(.horizontal, 18)
                    })
                    
                    // Next page to Name Plant View
                    NavigationLink(
                        destination: MyPlantsNameView(showNameView: $showChoosePlant, selectedPlantId: selectedId ?? 0, myPlantsVM: myPlantsVM),
                        label: {
                            Text("Continue")
                                .bold()
                                .padding(14)
                                .padding(.horizontal, 45)
                                .foregroundColor(Color.white)
                                .background(selectedId == nil ? Color(UIColor.systemGray2) : Color("primaryGreen"))
                                .cornerRadius(25)
                                .shadow(color: Color(UIColor.systemGray2), radius: 2, x: 0, y: 1.5)
                        })
                        .disabled(selectedId == nil ? true : false)
                        .padding(.top, 20)
                }
                .navigationTitle("Plant Type")
                .toolbar(content: {
                    ToolbarItem(placement: .navigationBarLeading) {
                        Button(action: {
                            showChoosePlant = false
                        }, label: {
                            Text("Cancel")
                        })
                    }
                })
                .navigationBarBackButtonHidden(true)
                .navigationBarTitleDisplayMode(.inline)
            }
        }
    }
}
