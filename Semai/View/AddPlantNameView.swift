//
//  AddPlantNameView.swift
//  Semai
//
//  Created by Octavianus Bastian on 29/07/21.
//

import SwiftUI
import Combine

struct AddPlantNameView: View {
    @Environment(\.presentationMode) var presentationMode
    @State var plantName = ""
    let nameLimit = 30
    @Binding var showNameView: Bool
    var selectedPlantId: Int
    @ObservedObject var myPlantsVM: MyPlantsViewModel

    var body: some View {
        ZStack {
            Color(#colorLiteral(red: 0.9725490196, green: 0.9725490196, blue: 0.9725490196, alpha: 1)).ignoresSafeArea()
            VStack {
                Text("Give name to your plant")
                    .bold()
                    .padding(.bottom, 50)
                CustomTextField(text: $plantName, isFirstResponder: true)
                    .padding(7)
                    .padding(.horizontal, 14)
                    .background(Color.white)
                    .cornerRadius(8)
                    .shadow(color: Color(UIColor.systemGray2), radius: 2, x: 0, y: 1)
                    .overlay(
                        HStack {
                            Spacer()
                                // button to reset textfield
                                Button(action: {
                                    self.plantName = ""
                                }) {
                                    Image(systemName: "multiply.circle.fill")
                                        .foregroundColor(Color("primaryGreen"))
                                        .padding(.trailing, 8)
                                }
                        }
                    )
                    .padding(.horizontal, 25)
                    .frame(width: UIScreen.screenWidth - 20, height: 45)
                    .onReceive(Just(plantName), perform: { _ in
                        textLimit(nameLimit)
                    })
                // Back button and continue button
                HStack (spacing: 25) {
                    Button(action: {self.presentationMode.wrappedValue.dismiss()}, label: {
                        Text("Back")
                            .bold()
                            .frame(minWidth: 0, maxWidth: .infinity)
                    })
                    .buttonStyle(SecondaryButton.init())
                    NavigationLink(
                        destination: EquipmentView(showEquipmentView: $showNameView),
                        label: {
                            Text("Continue")
                                .bold()
                                .frame(minWidth: 0, maxWidth: .infinity)
                                .padding(14)
                                .foregroundColor(Color.white)
                                .background(plantName.isEmpty ? Color(UIColor.systemGray2) : Color("primaryGreen"))
                                .cornerRadius(25)
                                .shadow(color: Color(UIColor.systemGray2), radius: 2, x: 0, y: 1.5)
                        })
                        .disabled(plantName.isEmpty ? true : false)
                        .simultaneousGesture(TapGesture().onEnded({
                            myPlantsVM.addMyPlants(plantType: selectedPlantId, plantName: plantName)
                        }))
                }
                .padding(.top, 50)
                .padding(.horizontal, 40)
                Spacer()
            }
        }
        .navigationBarBackButtonHidden(true)
        .navigationTitle("Plant Type")
        .toolbar(content: {
            ToolbarItem(placement: .navigationBarLeading) {
                Button(action: {
                    showNameView = false
                }, label: {
                    Text("Cancel")
                })
            }
        })
    }
    
    func textLimit(_ upper: Int) {
        if plantName.count > upper {
            plantName = String(plantName.prefix(upper))
        }
    }
}

//struct AddPlantNameView_Previews: PreviewProvider {
//    static var previews: some View {
//        AddPlantNameView()
//    }
//}
