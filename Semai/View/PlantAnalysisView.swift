//
//  PlantAnalysisView.swift
//  Semai
//
//  Created by Adhitya Laksamana Bayu Adrian on 30/07/21.
//

import SwiftUI

struct PlantAnalysisView: View {
    var body: some View {
        NavigationView{
            VStack{
                Image("boxPrepare")
                    .resizable()
                    .scaledToFit()
                    .cornerRadius(10)
                    .padding(.horizontal, 25).frame(height:300)
                    
                    
                Text("Plant Care")
                Text("Plant Analysis")
                HStack{
                    Button(action: /*@START_MENU_TOKEN@*/{}/*@END_MENU_TOKEN@*/, label: {
                        Text("Back")
                            .bold()
                            .padding(14)
                            .padding(.horizontal)
                            .foregroundColor(.white)
                            .background(Color("primaryGreen"))
                            .cornerRadius(25)
                    })
                    Button(action: /*@START_MENU_TOKEN@*/{}/*@END_MENU_TOKEN@*/, label: {
                        Text("Continue")
                            .bold()
                            .padding(14)
                            .padding(.horizontal)
                            .foregroundColor(.white)
                            .background(Color("primaryGreen"))
                            .cornerRadius(25)
                    })
                }
                
                
                
            }
        }
    }
}

struct PlantAnalysisView_Previews: PreviewProvider {
    static var previews: some View {
        PlantAnalysisView()
    }
}
