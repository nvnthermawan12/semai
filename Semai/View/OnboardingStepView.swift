//
//  OnboardingStepView.swift
//  Semai
//
//  Created by Octavianus Bastian on 25/07/21.
//

import SwiftUI

struct OnboardingStepView: View {
    var data: OnBoardingManager
    
    var index: Int
    
    var body: some View {
        ZStack {
            ZStack {
                Image(data.onBoardings[index].image)
                    .resizable()
                    .scaledToFit().edgesIgnoringSafeArea(.all)
            }.padding(.top, -300)
            
            VStack {
                Text(data.onBoardings[index].heading)
                    .font(.title)
                    .fontWeight(.bold)
                    .foregroundColor(Color.white)
                Spacer()
                Spacer()
                Text(data.onBoardings[index].text)
                    .font(.body)
                    .fontWeight(.regular)
                    .multilineTextAlignment(.center)
                    .padding(.horizontal, 35)
                    .padding(.top, 50)
                Spacer()
            }
            .padding(.top, 40)
        }
        
        
        
    }
}

struct OnboardingStepView_Previews: PreviewProvider {
    static var data = OnBoardingManager()
    static var previews: some View {
        OnboardingStepView(data: data, index: 0)
    }
}
