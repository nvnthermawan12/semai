//
//  SemaiApp.swift
//  Semai
//
//  Created by Novianto Hermawan on 16/07/21.
//

import SwiftUI

@main
struct SemaiApp: App {
    @Environment(\.scenePhase) var scenePhase
    let persintenceController = PersistenceController.shared
//    let nutritionPersistence = AddNutritionPersistence.shared
    var body: some Scene {
        WindowGroup {
            MyPlantsView()
                .environment(\.managedObjectContext, persintenceController.container.viewContext)
        }
        .onChange(of: scenePhase) { _ in
            persintenceController.save()
        }
    }
}
