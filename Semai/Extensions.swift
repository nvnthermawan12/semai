//
//  Extensions.swift
//  Semai
//
//  Created by Felinda Gracia Lubis on 26/07/21.
//

import SwiftUI

extension UINavigationController {
    open override func viewDidLoad() {
        let appearance = UINavigationBarAppearance()
        appearance.backgroundColor = UIColor(named:"primaryGreen")
        appearance.largeTitleTextAttributes = [.foregroundColor: UIColor.white]
        appearance.titleTextAttributes = [.foregroundColor: UIColor.white]
        navigationBar.scrollEdgeAppearance = appearance
        navigationBar.compactAppearance = appearance
        navigationBar.standardAppearance = appearance
        navigationBar.tintColor = .white
    }
}

extension UIScreen {
   static let screenWidth = UIScreen.main.bounds.size.width
   static let screenHeight = UIScreen.main.bounds.size.height
   static let screenSize = UIScreen.main.bounds.size
}
