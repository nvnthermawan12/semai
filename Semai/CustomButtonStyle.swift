//
//  CustomButtonStyle.swift
//  Semai
//
//  Created by Felinda Gracia Lubis on 25/07/21.
//

import SwiftUI

struct SecondaryButton: ButtonStyle {
    func makeBody(configuration: Configuration) -> some View {
        configuration.label
            .padding(14)
            //            .frame(minWidth: 0, maxWidth: .infinity)
            .foregroundColor(Color("primaryGreen"))
            .background(Color.white)
            .cornerRadius(25)
            .shadow(color: Color(UIColor.systemGray2), radius: 2, x: 0, y: 1.5)
    }
}

struct PrimaryButton: ButtonStyle {
    func makeBody(configuration: Configuration) -> some View {
        configuration.label
            .padding(14)
            //            .frame(minWidth: 0, maxWidth: .infinity)
            .foregroundColor(Color.white)
            .background(Color("primaryGreen"))
            .cornerRadius(25)
            .shadow(color: Color(UIColor.systemGray2), radius: 2, x: 0, y: 1.5)
    }
}

struct DisabledButton: ButtonStyle {
    func makeBody(configuration: Configuration) -> some View {
        configuration.label
            .padding(14)
            .foregroundColor(Color.white)
            .background(Color(UIColor.systemGray2))
            .cornerRadius(25)
            .shadow(color: Color(UIColor.systemGray2), radius: 2, x: 0, y: 1.5)
    }
}


